import sys

table_name = sys.argv[1]

new_table = open(table_name, 'r')
new_lines = new_table.readlines()
new_table.close()

number_list = []

for i, rows in enumerate(new_lines[1:]):
    elements = rows.split("\n")[0]
    for j in elements.split(","):
        number_list.append(int(j))

print("list of numbers: ", number_list)
print("their sum : ", sum(number_list)/len(number_list))

