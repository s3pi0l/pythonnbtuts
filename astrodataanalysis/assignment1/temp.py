import sys

def celcius(F):

    C = (F-32)*(5/9)
    return C

x = float(sys.argv[1])

print("Temp in Celcius:", celcius(x))

