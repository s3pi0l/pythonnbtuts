import math

def median(L):
    n = len(sorted(L))
    if n%2 == 1:

        index = n//2
        median = sorted(L)[index]

    else:
        index = n//2 - 1
        median = (sorted(L)[index] + sorted(L)[index+1])/2

    return median


def mean(L):
    total = 0
    for i in L:
        total+=i
    mean = total/len(L)
    return mean

def stddev(L, mean):
    var = 0
    for j in L:
        var+= (mean-j)**2
    stddev = math.sqrt(var/(len(L)-1))

    return stddev
